package com.Malashnyak.XML;

import com.Malashnyak.XML.Models.Sweet;
import com.Malashnyak.XML.Models.SweetComparator;
import com.Malashnyak.XML.Parsers.DOM;
import com.Malashnyak.XML.Parsers.SAX.SAX;
import com.Malashnyak.XML.Parsers.StAX;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {
  public static void main(String[] args) {
    File xml = new File("src\\main\\resources\\Sweets.xml");
    File xsd = new File("src\\main\\resources\\sweetsXSD.xsd");

    System.out.println("SAX");
    SAX sax = new SAX();
    List<Sweet> SAXSweets = new ArrayList<>();
    Collections.sort(SAXSweets, new SweetComparator());
    SAXSweets = sax.SAXGetSweets(xml, xsd);
    for (Sweet sw : SAXSweets) {
      System.out.println(sw.toString());
    }

    System.out.println("StAX");
    List<Sweet> StAXSweets = StAX.getSweets(xml);
    Collections.sort(StAXSweets, new SweetComparator());
    for (Sweet sw : StAXSweets) {
      System.out.println(sw.toString());
    }

    DOM dom = new DOM();
    System.out.println("DOM:");
    List<Sweet> sweets = dom.getSweets(xml);
    Collections.sort(sweets, new SweetComparator());
    for (Sweet sw : sweets) {
      System.out.println(sw.toString());
    }
  }
}
