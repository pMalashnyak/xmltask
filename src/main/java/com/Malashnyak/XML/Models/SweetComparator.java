package com.Malashnyak.XML.Models;

import java.util.Comparator;

public class SweetComparator implements Comparator<Sweet> {
  @Override
  public int compare(Sweet o1, Sweet o2) {
    return Integer.compare(o1.getEnergy(), o2.getEnergy());
  }
}
