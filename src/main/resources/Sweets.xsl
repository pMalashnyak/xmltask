<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <head></head>
            <body>
                <table>
                    <tr>
                        <th style="width:250px">Name</th>
                        <th style="width:250px">Energy</th>
                        <th style="width:250px">Type</th>
                        <th style="width:250px">Ingredients</th>
                        <th style="width:250px">Proteins</th>
                        <th style="width:250px">Fat</th>
                        <th style="width:250px">Carbohydrates</th>
                        <th style="width:250px">Production</th>
                    </tr>
                    <xsl:for-each select="sweets/sweet">
                        <tr>
                            <td>
                                <xsl:value-of select="name"/>
                            </td>
                            <td>
                                <xsl:value-of select="energy"/>
                            </td>
                            <td>
                                <xsl:value-of select="type"/>
                            </td>
                          <!--  <td>
                                <xsl:for-each select="ingredients/ingredient">
                                    <xsl:value-of select="ingredient"/>
                                </xsl:for-each>
                            </td>-->
                            <td>
                                <xsl:value-of select="value/proteins"/>
                            </td>
                            <td>
                                <xsl:value-of select="value/fat"/>
                            </td>
                            <td>
                                <xsl:value-of select="value/carbohydrates"/>
                            </td>
                            <td>
                                <xsl:value-of select="production"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>